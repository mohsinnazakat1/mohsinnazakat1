### Hi there, I'm Mohsin Nazakat 👋

## I'm a Student of Computer Science and areas of my intrest are

- Machine Learning
- Data Science


## More About me

- 🔭 I'm Currently working on Machine Learning | Toxicity in speech | Audio Signal Processing |
- 🌱 I’m currently learning a lot of things 
- 👯 I’m looking to collaborate with other Machine Learning Enthusiasts. 
- 🥅 2022 Goals: Keep Learning and Keep Sharing

### Connect with me:

[<img align="left" alt="codeSTACKr.com" width="22px" src="https://raw.githubusercontent.com/iconic/open-iconic/master/svg/globe.svg" />][website]
[<img align="left" alt="codeSTACKr | Twitter" width="22px" src="https://cdn.jsdelivr.net/npm/simple-icons@v3/icons/twitter.svg" />][twitter]
[<img align="left" alt="codeSTACKr | LinkedIn" width="22px" src="https://cdn.jsdelivr.net/npm/simple-icons@v3/icons/linkedin.svg" />][linkedin]
[<img align="left" alt="codeSTACKr | Instagram" width="22px" src="https://cdn.jsdelivr.net/npm/simple-icons@v3/icons/instagram.svg" />][instagram]

[website]: https://mohsinnazakat.com
[linkedin]: https://linkedin.com/in/mohsinnazakat11
[twitter]: https://twitter.com/mohsinnazakat1
[instagram]: https://instagram.com/mohsinnazakat11
